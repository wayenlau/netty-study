package com.wayen.bio;

import java.net.Socket;
import java.util.Date;

public class BIOClientThread extends Thread {

    @Override
    public void run() {

        try {
            Socket socket = new Socket("127.0.0.1", 2221);
            while (true) {
                System.out.println(getName());
                socket.getOutputStream().write((new Date() + ": hello-world").getBytes());
                Thread.sleep(10000);
            }
        } catch (Exception e) {
        }
    }
}
