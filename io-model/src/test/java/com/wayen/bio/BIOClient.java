package com.wayen.bio;

import java.io.IOException;
import java.net.Socket;
import java.util.Date;

/**
 * BIO(同步阻塞io模型客户端)
 */
public class BIOClient {

    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());
        new BIOClientThread().start();

    }

}
