package com.wayen.bio;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class BIOServer {

    public static void main(String[] args) throws IOException {

        System.out.println(Thread.currentThread().getName());
        final ServerSocket serverSocket = new ServerSocket(2221);
        new Thread(new Runnable() {
            public void run() {
                System.out.println(Thread.currentThread().getName());
                while (true) {
                    try {
                        final Socket socket = serverSocket.accept();
                        new Thread(new Runnable() {
                            public void run() {

                                System.out.println(Thread.currentThread().getName());
                                int len;
                                byte[] data = new byte[1024];
                                try {
                                    InputStream inputStream = socket.getInputStream();
                                    // 按字节流方式读取数据
                                    while ((len = inputStream.read(data)) != -1) {
                                        System.out.println(new String(data, 0, len));
                                    }
                                } catch (IOException e) {
                                }

                            }
                        }).start();

                    } catch (Exception e) {
                    }
                }
            }
        }).start();
    }
}
