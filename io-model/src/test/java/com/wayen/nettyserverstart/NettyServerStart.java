package com.wayen.nettyserverstart;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

/**
 * netty服务端启动流程
 * 创建一个引导类，
 * 然后给他指定线程模型，IO 模型，连接读写处理逻辑，
 * 版定上特定主机和端口号服务端就启动起来了
 */
public class NettyServerStart {

    public static void main(String[] args) {

        //引导我们进行服务端的启动工作
        ServerBootstrap serverBootstrap = new ServerBootstrap();

        //监听端口，接受新连接
        NioEventLoopGroup boss = new NioEventLoopGroup();
        //处理每一条连接的数据读写
        NioEventLoopGroup worker = new NioEventLoopGroup();
        serverBootstrap
                .group(boss, worker)
                //指定io模型
                .channel(NioServerSocketChannel.class)
                //定义业务逻辑处理(知道处理新连接数据的读写处理逻辑)
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    protected void initChannel(NioSocketChannel ch) {
                        ch.pipeline().addLast(new StringDecoder());
                        ch.pipeline().addLast(new SimpleChannelInboundHandler<String>() {
                            @Override
                            protected void channelRead0(ChannelHandlerContext ctx, String msg) {
                                System.out.println(msg);
                            }
                        });
                    }
                })
                .bind(8000)
                //监听端口是否绑定成功
                .addListener(new GenericFutureListener<Future<? super Void>>() {
                    public void operationComplete(Future<? super Void> future) {
                        if (future.isSuccess()) {
                            System.out.println("端口绑定成功!");
                        } else {
                            System.err.println("端口绑定失败!");
                        }
                    }
                });
    }
}
